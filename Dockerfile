FROM python:3.10-slim

ARG module=myapp
ARG USER=user

# Set environment variables
ENV HOME /home/$USER
ENV PATH $HOME/venv/bin:$HOME/.local/bin:$PATH

# Create a non-root user
RUN useradd -m $USER
RUN mkdir -p $HOME && chown -R $USER:$USER $HOME

# Copy application files
USER $USER
COPY --chown=$USER:$USER --chmod=777 start.sh $HOME/
COPY --chown=$USER:$USER pyproject.toml README.md $HOME/src/
COPY --chown=$USER:$USER ./$module $HOME/src/$module

# Create virtual environment
RUN python -m venv $HOME/venv

# Install dependencies
RUN pip install --editable $HOME/src/

WORKDIR $HOME

# Run the application
CMD [ "/home/user/start.sh" ]
